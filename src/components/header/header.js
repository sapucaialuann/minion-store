import React from 'react';
import './header.css';
import IconCart from '../img/IconCart.svg';
import { Link } from 'react-router-dom';


function Header() {
  return (
    <header className="HeaderBg">
      <Link to="/" className="HeaderLogo">minionStore</Link>
      <div className="HeaderMenu">
        <div className="HeaderCart">
          <Link to="/cart" className="CartContainer"><img src={IconCart}/> <p>R$0,00</p></Link>
          {/* Aqui neste valor entra a quantidade no carrinho */}
        </div>
        <div className="HeaderOpt">
          <Link to="/contato" className="HeaderOptAnchor">Contato</Link>
          <Link to="/" className="HeaderOptAnchor">Registre-se</Link>
          <Link to="/" className="HeaderOptAnchor">login</Link>
        </div>
      </div>
    </header>
  );
}

export default Header;