import React from 'react';
import './cart.css';
import './emptycart.css';
import Header from '../header/header';
import Footer from '../footer/footer';
// import {productScore} from '../product/product';
// import Header from "../header/header ";
// import Footer from "../footer/footer ";
import surprisedMinions from "../img/surprisedMinions.svg";
import { Link } from 'react-router-dom';

function Cart() {
      const prodScore = 1;
      if (prodScore > 0) {
            return (
            <div>
                  <Header />
                  <div className="cartAll">
                        <div className="cartProducts">
                        <p className="summary">Quantidade</p>
                        <p className="summary">Unitário</p>
                        <p className="summary">Total</p>
                        </div>
                        <div className="cartSummary">
                              <p className="summary">Resumo</p>
                              <div className="summaryBoard">
                                          <p>Subtotal:</p>
                                          <p>Frete:</p>
                                          <p>Desconto:</p>
                                          <p className="summaryBoardTotal">Total:</p>
                              </div>
                        </div>
                  </div> 
                  <Footer />
            </div>      
            );
      }
      else {
            return (
                  <div>
                        <Header />
                        <div className="emptyCartAll">
                              <h1>Não há nenhum item no carrinho :(</h1>
                              <a href="#">Voltar</a>
                              <img src={surprisedMinions}/>
                        </div>
                        <Footer />
                  </div>
            );
      };
}
export default Cart;