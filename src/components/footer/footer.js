import React from 'react';
import './footer.css';
import IconFacebook from '../img/IconFacebook.svg';
import IconInstagram from '../img/IconInstagram.svg';


function Footer() {
  return (
    <div className="Footer">
      <div className="FooterSocial">
        <a href="#" className="Face"><img src={IconFacebook} /></a>
        <a href="#" className="Insta"><img src={IconInstagram} /></a>
      </div>
      <footer className="FooterCopyright">Copyright &copy; 2019 - All rights reserved</footer>
    </div>
  );
}

export default Footer;