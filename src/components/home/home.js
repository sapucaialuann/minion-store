import React from 'react';
import './home.css';
import Product from '../product/product.js';
import Header from '../header/header';
import Footer from '../footer/footer';
import { Link } from 'react-router-dom';

function Home() {
      return (
      <div>
            <Header />
            <div className="HomeContainer">
                  <Product />
            </div>
            <Footer />
      </div>
      );
}

export default Home;