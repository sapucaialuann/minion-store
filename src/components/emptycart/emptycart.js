import React from 'react';
import './emptycart.css';
import surprisedMinions from "../img/surprisedMinions.svg";


function EmptyCart() {
      return (
            <div className="emptyCartAll">
                  <h1>Não há nenhum item no carrinho :(</h1>
                  <a href="#">Voltar</a>
                  <img src={surprisedMinions}/>
            </div>
      );
}
export default EmptyCart;