import React from 'react';
import { useState } from "react";
import './product.css';
import MinionModel from '../img/MinionModel.svg';
import iconPlus from '../img/iconPlus.svg';
import iconMinus from '../img/iconMinus.svg';
import { Link } from 'react-router-dom';
import { pathToFileURL } from 'url';




function Product() {
      // let ProductScore = 1;
      const[productScore, setProductScore] = useState(1);
      const addProductScore = () => {
            if (productScore < 99) {
                  setProductScore(productScore + 1);
            }
      }
      const subtractProductScore = () => {
            if (productScore > 1) {
                  setProductScore(productScore - 1);
            }
      }

      if (productScore < 1) {
            setProductScore(1);
      }
      else if (productScore > 99) {
            setProductScore(99);
      }

      const productPriceTotal = 81 * productScore;
      // const minions = {
      //       "minion": {
      //             name: 'One Eye',
      //             description: 'Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis.                  Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis',
      //             picture: {MinionModel},
      //             price: '81.00'
      //       }
      // }


      return (
            <div className="productContainer">
                  <div className="homeProduct">
                        {/*Essa parte vai se tornar um componente para o produto ligado diretamente a API*/}
                        <img className="productImg" src={MinionModel}/>
                        <div className="productDetails">
                              <div className="productText">
                                    <p className="productTextTitle">One Eye</p>
                                    <p className="productTextDescription">'Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis.                  Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis'</p>
                              </div>
                              <div className="productBuy">
                                    <div className="productAmount">
                                          <p>Quantidade:</p>
                                          <div className="productAmountDetails">
                                                <input className="productAmountScore" onChange={(event) => setProductScore(Number(event.target.value))} value={productScore}/>
                                                <img type="image" src={iconPlus} className="productAmountOperation" onClick={addProductScore}/>
                                                <img type="image" src={iconMinus} className="productAmountOperation" onClick={subtractProductScore}/>
                                          </div>
                                    </div>
                                    <div className="productPrice">
                                          <p className="productPriceTotal">Total:</p>
                                          <p>R$:{productPriceTotal},00</p>
                                          {/* ainda falta corrigir esse valor */}
                                    </div>
                              </div>
                              <button type="submit" className="buyButton">comprar</button>
                        </div>
                  </div>
            </div>
      );
}

export default Product;