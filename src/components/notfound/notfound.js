import React from 'react';
import './notfound.css';
import Header from "../header/header";
import Footer from "../footer/footer";
import evilMinion from "../img/evilMinion.svg";


function NotFound() {
      return (
      <div>
            <Header />
            <div className="notFoundAll">
                  <div className="notFoundText">
                        <h1>404</h1>
                        <h2>Não há nada aqui!</h2>
                  </div>
                  <img src={evilMinion}/>
            </div>
            <Footer />
      </div>
      );
}
export default NotFound;