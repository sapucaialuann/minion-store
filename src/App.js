import React from 'react';
import './components/reset.css';
import './App.css';

function App() {
  return (
    <div>
      <Header />

      <Footer />
    </div>
  );
}

export default App;
